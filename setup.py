import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fileshare_client",
    version="0.0.1",
    author="Pierre CHÉNÉ",
    author_email="pierre.chene44@gmail.com",
    description="FileShare trick for watchdog",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=['fileshare_client'],
    install_requires=[
        'pyyaml',
        'argh',
        'requests',
        'pathtools'
    ],
    setup_requires=['wheel'],
    python_requires='>=3.6',
)