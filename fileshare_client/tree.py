import os 
from .entities import *
from .encoders import ComplexEncoder
from pathtools.patterns import match_path_against
from .exceptions import MissingDSNExceptionError
from .request_maker import TokenAndUrl
from .utils import get_file_share_file_content

class Tree:
    ROOT = "."
    FILESHARE_JSON_FILE = ".fileshare.json"
    def __init__(self, name=None, exclude_patterns=[], ignore_directories=None, dsn=None):
        self.dsn = dsn
        
        self.exclude_patterns = exclude_patterns
        self.ignore_directories = ignore_directories

        self.classroom = None

        self.original_directory_name = name if name else self.ROOT
        self.fileshare_json_file = os.path.join(self.original_directory_name, self.FILESHARE_JSON_FILE)


    def init_tree(self):
        if self.dsn:
            TokenAndUrl(dsn=self.dsn)
        else:
            raise MissingDSNExceptionError()

        if os.path.isfile(self.fileshare_json_file):
            # If the file already exists and is in sync with the existing files
            self.classroom, self.original_directory = get_file_share_file_content(self.fileshare_json_file)

        if not self.classroom:
            self.classroom = Classroom()
            if not self.classroom.create():
                print("Unable to create a classroom on the server, verify your token")
                os._exit(0)
            self.original_directory = Dossier(
                name=self.original_directory_name,
                classroom=self.classroom,
                is_root=True
            )
            
            self.generate_tree()  

            self.generate_composants_on_server()

            self.set_entities_properties_from_server()
            self.init_fileshare_repo() 

        print(f"The classroom identifier is : {self.classroom.random_slug}")
        
    def __str__(self):
        return str(self.original_directory)

    def init_fileshare_repo(self):
        if os.path.isfile(self.fileshare_json_file):
            # It's completely temporary
            os.remove(self.fileshare_json_file)
            self.init_fileshare_repo()
            # TODO : compare file and tree freshly generated
        else:
            with open(self.fileshare_json_file, "w") as f:
                f.write(self.generate_file_content())
    
    def get_element_by_name(self, name, directory=None):

        if self.original_directory.name == name:
            return self.original_directory
        directory = directory if directory else self.original_directory
        for x in directory.list_children:
            if x.name == name:
                return x
            elif isinstance(x, Dossier):
                found = self.get_element_by_name(name, directory=x)
                if found:
                    return found
        
        return None
            
    def get_parent_by_name(self, name):
        element = self.get_element_by_name(name)
        if element:
            return element.parent
        else:
            return None
    
    def get_new_parent_by_name(self, name):
        """
        This method is used for components that does not have a parent yet
        """
        found = self.get_element_by_name(os.path.dirname(name))
        if found:
            return found
        else:
            return None

    def generate_file_content(self):
        return ComplexEncoder().encode(self)
    
    def reprJSON(self):
        return {
            "classroom": self.classroom,
            "tree": self.original_directory
        }

    def generate_tree(self):
        self.generate_dir(self.original_directory)
        
    def generate_dir(self, parent):
        for i in os.listdir(parent.name):
            if i in self.exclude_patterns:
                continue
            composant = self.generate_node(parent, i)
            if composant:
                composant.set_parent(parent)
    
    def generate_node(self, parent, composant):
        name = os.path.join(parent.name, os.path.basename(composant))
        if match_path_against(name, self.exclude_patterns, True):
            return None
        if not self.ignore_directories and os.path.isdir(name):
            dossier = Dossier(name, classroom=self.classroom)
            self.generate_dir(dossier)
            return dossier
        elif os.path.isfile(name):
            return Fichier(name, classroom=self.classroom)
        else:
            return None
        
    def generate_composants_on_server(self):
        self.original_directory.generate_directory_on_server()
    
    def set_entities_property_for_directory(self, directory, list_children):
        for child in list_children:
            child_entity = directory.get_child_by_name(child["name"])
            child_entity.id = child["id"]
            
            if isinstance(child_entity, Fichier):
                child_entity.filename_hashed = child['filename_hashed']
            
            if isinstance(child_entity, Dossier):
                self.set_entities_property_for_directory(child_entity, child['children'])
    
    def set_entities_properties_from_server(self):
        json_response = self.classroom.get_project()
        if json_response:
            original_directory_json = json_response['project']
            self.original_directory.id = original_directory_json['id']
            self.set_entities_property_for_directory(self.original_directory, original_directory_json['children'])
            
    