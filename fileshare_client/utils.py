import os
import json
from .entities import Classroom, Dossier, Fichier

def get_file_share_file_content(filename):
    """
    This function loads the .fileshare.json file and returns
    the classroom entity and the tree written in the file
    """
    try:
        with open(filename, 'r') as f:
            content = json.loads(f.read())
            classroom = Classroom(**content["classroom"])

            tree = generate_node_from_dict(content["tree"], classroom)
            return [classroom, tree]
    except:
        # The file does not exist
        return None

def generate_node_from_dict(dict, classroom):
        composant = None
        if dict['type'] == 'directory':
            composant = Dossier(id=dict["id"], name=dict["name"], level=dict["level"], classroom=classroom)
            if len(dict["list_children"]) > 0:
                for child in dict["list_children"]:
                    child_component = generate_node_from_dict(child, classroom)
                    child_component.set_parent(composant)
        elif dict['type'] == 'file':
            composant = Fichier(id=dict["id"], name=dict["name"], filename_hashed=dict["filename_hashed"], classroom=classroom)
        return composant