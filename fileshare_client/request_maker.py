
import requests
from urllib.parse import urljoin
from .exceptions import MissingDSNExceptionError, MisconfiguredDSNExceptionError


def singleton(class_):
    instances = {}

    def delinstance(*args, **kwargs):
        del instances[class_]

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance

@singleton
class TokenAndUrl:

    def __init__(self, dsn=None):
        if not dsn:
            raise MissingDSNExceptionError()
            
        try:
            self.url, self.token = dsn.split('@')
        except:
            raise MisconfiguredDSNExceptionError()

class HTTPObjectMixin:

    def __init__(self):
        self.base_url = TokenAndUrl().url
        self.token = TokenAndUrl().token
    
    def get_headers(self):
        return {'Authorization': f'Token {self.token}'}

    def get_url_with_id(self):
        return self.get_url() + str(self.id) + "/"
    
    def get_url(self):
        return f"{self.base_url}{self.URL}"

    def create(self):
        return requests.post(self.get_url(), data=self.serialized_to_be_created(), headers=self.get_headers())

    def update(self, *args, **kwargs):
        return requests.patch(self.get_url_with_id(), data=kwargs, headers=self.get_headers())

    def delete(self):
        response = requests.delete(self.get_url_with_id(), headers=self.get_headers())
        if response.status_code == 204:
            return True
        else:
            return False

    def serialized_to_be_created(self):
        pass