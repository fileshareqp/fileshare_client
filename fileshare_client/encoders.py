from json import JSONEncoder
from .entities import *
import json



class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj,'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)

class ComplexCreateEntitiesEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj,'serialized_to_be_created_grouped'):
            return obj.serialized_to_be_created_grouped()
        else:
            return json.JSONEncoder.default(self, obj)
