class MissingDSNExceptionError(Exception):
    """Raised when the token is not defined in tricks.yml
    """

    def __init__(self):
        self.message = "Le token doit être défini dans le fichier tricks.yml"

class MisconfiguredDSNExceptionError(Exception):
    """Raised when the token is not defined in tricks.yml
    """

    def __init__(self):
        self.message = "Le dsn doit contenir l'url et le token séparés d'un @"