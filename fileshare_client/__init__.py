import os
from watchdog.tricks import Trick
from watchdog.events import LoggingEventHandler
import requests

from .entities import Fichier, Dossier

from .tree import Tree

import logging


class FileShareTrick(Trick):

    ROOT = "."
    def __init__(self, patterns=None, ignore_patterns=None,
                 ignore_directories=False, case_sensitive=False, hash_names=None, dsn=None):
        
        fileshare_json_file = "./.fileshare.json"
        if not ignore_patterns:
            ignore_patterns = [fileshare_json_file]
        else:
            ignore_patterns.append(fileshare_json_file)
        super(FileShareTrick, self).__init__(patterns, ignore_patterns, ignore_directories, case_sensitive)
        self.tree = Tree(name=self.ROOT, exclude_patterns=ignore_patterns, ignore_directories=ignore_directories, dsn=dsn)
        self.tree.init_tree()

    def on_created(self, event):
        composant = None
        if event.is_directory:
            composant = Dossier(event.src_path, classroom=self.tree.classroom)
            composant.create()
        else:
            try:
                composant = Fichier(event.src_path, classroom=self.tree.classroom)                
                composant.create()
            except UnicodeDecodeError:
                # The file does not contain text
                pass
        if composant:
            parent_dir = self.tree.get_new_parent_by_name(event.src_path)
            if parent_dir:
                composant.set_parent(parent_dir)
                composant.update(parent=parent_dir.id)

        self.tree.init_fileshare_repo()

    def on_deleted(self, event):
        composant = self.tree.get_element_by_name(event.src_path)
        if composant:
            composant.delete()
            self.tree.init_fileshare_repo()
        else:
            print(f"unable to delete file {event.src_path}")

    def on_modified(self, event):
        composant = self.tree.get_element_by_name(event.src_path)
        if composant:
            if  event.is_directory:
                pass
            else:
                composant.update(content=composant.content)
                self.tree.init_fileshare_repo()
        
    def on_moved(self, event):
        composant = self.tree.get_element_by_name(event.src_path)
        if composant:
            composant.update(name=event.dest_path)
            parent = self.tree.get_new_parent_by_name(event.dest_path)
            composant.set_parent(parent)
            composant.update(parent=parent.id)
            self.tree.init_fileshare_repo()