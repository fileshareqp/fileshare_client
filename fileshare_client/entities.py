import hashlib
import requests
from .request_maker import HTTPObjectMixin
from fileshare_client.encoders import ComplexCreateEntitiesEncoder



class Classroom(HTTPObjectMixin):
    URL = "/api/classrooms/"
    def __init__(self, id=None, random_slug=None, *args, **kwargs):
        super().__init__(**kwargs)
        self.id = id
        self.random_slug = random_slug
        self.owner = None
    
    def create(self):
        response = super().create()
        if response.status_code == 201:
            json_response = response.json()
            self.id = json_response['id']
            self.random_slug = json_response['random_slug']
            return True
        return False
    
    def get_project(self):
        response = requests.get(self.get_url_with_id(), headers=self.get_headers())
        if response.status_code == 200:
            return response.json()
        return None
    
    def reprJSON(self):
        return {
            "id":self.id,
            "random_slug": self.random_slug
        }
    
    def serialized_to_be_created(self):
        return {}

class Composant(HTTPObjectMixin):
    PRE = "-"
    def __init__(self, classroom=None, name=None, id=None, level=None, **kwargs):
        super().__init__(**kwargs)
        self.id = id
        self.classroom = classroom
        self.name = name
        self.level=0
        self.parent = None
    
    def set_parent(self, parent):
        self.parent = parent
        self.level = self.parent.level + 1
        parent.list_children.append(self)
    
    def __str__(self):
        space = self.level * ' '
        return space + self.PRE + self.name +"\n"

    def reprJSON(self):
        return {
            "id":self.id,
            "name":self.name,
            "level": self.level,
            "classroom": self.classroom.id if self.classroom else None,
            "type": self.TYPE
        }
    

class Fichier(Composant):
    URL = "/api/fichiers/"
    PRE = "+"
    TYPE = "file"

    def __init__(self, name=None, id=None, filename_hashed=None, classroom=None, **kwargs):
        super().__init__(name=name, id=id, classroom=classroom, **kwargs)
        self.filename_hashed = filename_hashed
        self.is_not_text = False
    
    def create(self):
        response = super().create()
        if response.status_code == 201:
            json_response = response.json()
            self.id = json_response['id']
            self.filename_hashed = json_response['filename_hashed']
            return True
        return False
    
    def update(self, *args, **kwargs):
        response = super().update(**kwargs)
        if kwargs.get('name', None):
            self.name = kwargs.get('name')
        
        return True
    
    def delete(self):
        super().delete()
        self.parent.list_children.remove(self)
        return True
    
    def get_content_from_remote(self):
        url = self.URL + "content/" +self.filename_hashed + "/"
        return requests.get(url, headers=self.get_headers()).json()
    
    def reprJSON(self):
        first_dict = super().reprJSON()
        content = self.content
        if self.content is None:
            self.is_not_text = True
        return {
            **first_dict, 
            "filename_hashed": self.filename_hashed,
            "hashed_content": self.hashed_content,
            "is_not_text": self.is_not_text
        }
    
    def serialized_to_be_created(self):
        content = self.content
        if self.content is None:
            self.is_not_text = True
        return {
            "name": self.name,
            "parent": self.parent.id if self.parent else None,
            "level": self.level,
            "content": self.content,
            "classroom": self.classroom.id if self.classroom else None,
            "hashed_content": self.hashed_content,
            "is_not_text": self.is_not_text
        }
    
    def serialized_to_be_created_grouped(self):
        content = self.content
        if self.content is None:
            self.is_not_text = True
        return {
            "name": self.name,
            "level": self.level,
            "content": self.content,
            "classroom": self.classroom.id if self.classroom else None,
            "type": "file",
            "is_not_text": self.is_not_text
        }
    
    @property
    def hashed_content(self):
        if not self.is_not_text:
            try:
                result = hashlib.md5(self.content.encode())
                return result.hexdigest()
            except:
                self.is_not_text = True
                return None
        else:
            return None
    
    @property
    def content(self):
        try:
            with open(self.name, 'r') as f:
                return f.read()
        except UnicodeDecodeError:
            self.is_not_text = True
            return None


class Dossier(Composant):
    URL = "/api/dossiers/"
    URL_ORIGINAL = "/api/original-dossier/"
    PRE = "*"
    TYPE = "directory"

    def __init__(self, name=None, id=None, classroom=None, is_root=False, **kwargs):
        super().__init__(name=name, id=id, classroom=classroom, **kwargs)
        self.list_children = []
        self.level = 0
        self.is_root = is_root
        
    def __str__(self):
        dossier_to_str = super().__str__()
        for x in self.list_children:
            dossier_to_str += str(x)
        
        return dossier_to_str
    
    def reprJSON(self):
        first_dict = super().reprJSON()
        return {
            **first_dict, 
            "list_children": self.list_children
        }
    
    def serialized_to_be_created(self):
        return {
            "name": self.name,
            "parent": self.parent.id if self.parent else None,
            "level": self.level,
            "classroom": self.classroom.id if self.classroom else None,
            "original_directory": self.is_root
        }
    
    def serialized_to_be_created_grouped(self):
        return {
            "name": self.name,
            "level": self.level,
            "classroom": self.classroom.id if self.classroom else None,
            "type": "directory",
            "original_directory": self.is_root,
            "list_children": self.list_children,
        }
    
    def create(self):
        response = super().create()
        if response.status_code == 201:
            json_response = response.json()
            self.id = json_response['id']
            return True
    
    def delete(self):
        super().delete()
        self.parent.list_children.remove(self)
        self.list_children = []
        return True
    
    def update(self, *args, **kwargs):
        super().update(**kwargs)
        if kwargs.get('name', None):
            self.name = kwargs['name']
        return True
    
    def get_child_by_name(self, name):
        for child in self.list_children:
            if child.name == name:
                return child
        
        return None
    
    def generate_directory_on_server(self):
        if self.is_root:
            data_to_send = ComplexCreateEntitiesEncoder().encode(self)
            headers = self.get_headers()
            headers['Content-type'] = "application/json"
            response = requests.post(f"{self.base_url}{self.URL_ORIGINAL}", data=data_to_send, headers=headers)
            if response.status_code == 201:
                return True
        return False