FileShareTrick
==============

Python trick used with [watchdog](https://github.com/gorakhargosh/watchdog) to make the FileShare happen

Installation
------------
Install from PyPI using ``pip``:

```bash
$ python3 -m pip install git+https://gitlab.com/fileshareqp/fileshare_client.git

$ python3 -m pip install  watchdog
```


Installation Caveats
--------------------

The ``watchmedo`` script depends on PyYAML_ which links with LibYAML_,
which brings a performance boost to the PyYAML parser. However, installing
LibYAML_ is optional but recommended. On Mac OS X, you can use homebrew_
to install LibYAML:

```bash
$ brew install libyaml
```

On Linux, use your favorite package manager to install LibYAML. Here's how you
do it on Ubuntu:

```bash
$ sudo aptitude install libyaml-dev
```

On Windows, please install PyYAML_ using the binaries they provide.

How to use?
-----------

This trick is just an extension of an existing library watchdog which comes with an utility called ``watchmedo``

To use the FileShare properly, you need to create a file named ``tricks.yml`` at the root of your directory.

Example tricks.yml
------------------

```yml
tricks:
- watchmedo_fileshare.FileShareTrick:
    ignore_patterns: ['.venv', '.git', '__pycache__', '.fileshare.json', 'tricks.yml']
    token: https://fileshare-app.bartradingx.com@c86e0d8d9b72c0fa7659f3974e835789266d6f38
``` 
Then, you can launch this command at the root of your directory

```bash
watchmedo tricks-from tricks.yml
```

Then, to stop the sharing, press ctr+C


About using the FileShare with editors like Vim
------------------------------------------
Vim does not modify files unless directed to do so.
It creates backup files and then swaps them in to replace
the files you are editing on the disk. This means that
if you use Vim to edit your files, the on-modified events
for those files will not be triggered by watchdog.
You may need to configure Vim appropriately to disable
this feature.


Dependencies
------------
1. Python 3.6 or above.