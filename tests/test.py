import unittest
import json
import os
from fileshare_client.request_maker import TokenAndUrl
from fileshare_client.utils import get_file_share_file_content
from fileshare_client.entities import *
from fileshare_client.tree import Tree
from fileshare_client.encoders import ComplexCreateEntitiesEncoder
from unittest.mock import patch

class TestDSN(unittest.TestCase):
    def test_dsn_creation(self):   
        TokenAndUrl(dsn="http://127.0.0.1:8000@c69abde10d4df7b680a8743827e03f12dc0dcdcb")

        self.assertEqual(TokenAndUrl().url, "http://127.0.0.1:8000")
        self.assertEqual(TokenAndUrl().token, "c69abde10d4df7b680a8743827e03f12dc0dcdcb")

class TestEntities(unittest.TestCase):

    def test_get_fileshare_content(self):
        classroom, original_directory = get_file_share_file_content('./tests/test_datas/.fileshare_simple.json')
        self.assertEqual(classroom.random_slug, "52edde4a-35b5-11eb-9dd5-0242ac130008")
        self.assertEqual(classroom.id, 6)

        self.assertEqual(len(original_directory.list_children), 1)
        self.assertEqual(original_directory.id, 319)
        self.assertEqual(original_directory.name, ".")
        self.assertEqual(original_directory.classroom.id, classroom.id)
    
    def test_get_file_content(self):
        fichier = Fichier('./tests/test_datas/essai.py')
        self.assertEqual('class Essai:\n\n    def je_teste(self):\n        return True', fichier.content)
        self.assertEqual('1b4494f33c7cd3d1aa590695fd678e82', fichier.hashed_content)
    
    def test_get_file_not_text(self):
        fichier = Fichier('./tests/test_datas/test_picture.png')
        self.assertEqual(None, fichier.content)
        self.assertEqual(True, fichier.is_not_text)

class IntegrationTestCase(unittest.TestCase):

    def test_create_directory(self):
        tree = Tree('./tests/test_datas/test_tree', dsn="http://127.0.0.1:8000@c69abde10d4df7b680a8743827e03f12dc0dcdcb")
        tree.init_tree()

        fichier = tree.get_element_by_name("./tests/test_datas/test_tree/essai/test_picture.png")
        self.assertTrue(fichier.is_not_text)

        os.remove("./tests/test_datas/test_tree/.fileshare.json")

class CreateEntitiesOnServerTestCase(unittest.TestCase):

    maxDiff = None
    def setUp(self):
        TokenAndUrl(dsn="http://127.0.0.1:8000@c69abde10d4df7b680a8743827e03f12dc0dcdcb")
        
    @patch('fileshare_client.entities.Fichier.content', 'bonjour')
    def test_payload_for_post_entities(self):
        classroom = Classroom(id=1)
        root = Dossier('.', is_root=True, classroom=classroom)

        file_1 = Fichier(name="./test.js", classroom=classroom)
        directory_1 = Dossier(name="./dossier", classroom=classroom)

        file_1.set_parent(root)
        directory_1.set_parent(root)

        file_2 = Fichier(name="./dossier/test.py", classroom=classroom)
        file_2.set_parent(directory_1)
        str_encoded = '{"name": ".", "level": 0, "classroom": 1, "type": "directory", "original_directory": true, "list_children": [{"name": "./test.js", "level": 1, "content": "bonjour", "classroom": 1, "type": "file", "is_not_text": false}, {"name": "./dossier", "level": 1, "classroom": 1, "type": "directory", "original_directory": false, "list_children": [{"name": "./dossier/test.py", "level": 2, "content": "bonjour", "classroom": 1, "type": "file", "is_not_text": false}]}]}'
        self.assertEqual(ComplexCreateEntitiesEncoder().encode(root), str_encoded)
    
    @patch('fileshare_client.entities.Classroom.get_project')
    def test_get_classroom_set_properties_on_entities(self, mocked_get_project):
        tree = Tree(dsn="http://127.0.0.1:8000@c69abde10d4df7b680a8743827e03f12dc0dcdcb")

        str_classroom = json.loads('{"id":1,"random_slug":"2db336a4-3a68-11eb-bb5a-0242ac120006","owner":1,"creation_date":"2020-12-09T21:47:48.266949Z","project":{"id":52,"name":".","parent":null,"level":0,"children":[{"id":53,"name":"./bonjour.py","parent":52,"level":1,"filename_hashed":"2dbe61f0-3a68-11eb-bb5a-0242ac120006","classroom":1,"instance_type":"file","hashed_content":null,"is_not_text":false},{"id":56,"name":"./test","parent":52,"level":1,"children":[{"id":57,"name":"./test/essai.js","parent":56,"level":2,"filename_hashed":"2df29a56-3a68-11eb-bb5a-0242ac120006","classroom":1,"instance_type":"file","hashed_content":null,"is_not_text":false}],"classroom":1,"instance_type":"directory"}],"classroom":1,"instance_type":"directory"}}')
        mocked_get_project.return_value = str_classroom
        
        classroom = Classroom(id=1, random_slug="2db336a4-3a68-11eb-bb5a-0242ac120006")
        tree.classroom = classroom
        
        root = Dossier(name=".", classroom=classroom)
        tree.original_directory = root
        
        file_1 = Fichier(name="./bonjour.py", classroom=classroom)
        directory_1 = Dossier(name="./test", classroom=classroom)
        file_1.set_parent(root)
        directory_1.set_parent(root)

        file_2 = Fichier(name="./test/essai.js", classroom=classroom)
        file_2.set_parent(directory_1)

        tree.set_entities_properties_from_server()

        self.assertEqual(root.id, 52)

        self.assertEqual(file_1.id, 53)
        self.assertEqual(file_1.filename_hashed, "2dbe61f0-3a68-11eb-bb5a-0242ac120006")

        self.assertEqual(directory_1.id, 56)
        
        self.assertEqual(file_2.id, 57)
        self.assertEqual(file_2.filename_hashed, "2df29a56-3a68-11eb-bb5a-0242ac120006")




if __name__ == '__main__':
    unittest.main()